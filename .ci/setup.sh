#!/usr/bin/env bash

set -o errexit

function install () {
    COMMAND=$1
    WHENCE=$2
    ZIP=${COMMAND}.zip

    which $COMMAND >/dev/null 2>&1 && echo "Already have ${COMMAND}." && return

    pushd /tmp

    curl -sL "${WHENCE}" -o ${ZIP}
    unzip -j ${ZIP}

    chmod 0755 ${COMMAND}
    cp ${COMMAND} /usr/local/bin

    popd
}

apt-get -yqq update
apt-get -yqq install curl zip openssh-client

install packer https://releases.hashicorp.com/packer/1.3.1/packer_1.3.1_linux_amd64.zip
install terraform https://releases.hashicorp.com/terraform/0.11.8/terraform_0.11.8_linux_amd64.zip
