#!/usr/bin/env bash

set -o errexit

pushd packer/gitlab-ci-runner
packer validate gitlab-ci-runner.json
popd

pushd packer/vault
packer validate vault.json
popd