#!/usr/bin/env bash

set -o nounset
set -o errexit

VERSION=1.0.1


until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
    sleep 1
done

sudo apt-get update -y

while sudo fuser -s /var/lib/dpkg/lock; do
    sleep 1
done

sudo apt-get install -y unzip

curl -L "https://releases.hashicorp.com/vault/${VERSION}/vault_${VERSION}_linux_amd64.zip" > /tmp/vault.zip

cd /tmp
sudo unzip vault.zip
sudo mv vault /usr/local/bin
sudo chmod 0755 /usr/local/bin/vault
sudo chown root:root /usr/local/bin/vault
