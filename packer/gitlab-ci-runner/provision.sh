#!/usr/bin/env bash

set -o nounset
set -o errexit

function no_exe() {
    ! command -v $1 >/dev/null 2>&1
}

RUNNER_HOME=/var/lib/gitlab-runner

# achieve quiescence
sleep 20

# update
sudo apt-get update -yqq

# install some prereqs.
echo "Installing prereqs..."
sudo apt-get install -yqq \
    aptitude \
    apt-transport-https \
    ca-certificates \
    curl \
    git \
    software-properties-common \
    unzip \
    build-essential libzmq3-dev libssl-dev protobuf-compiler gettext-base cmake pkg-config git clang libclang-dev
echo "Done installing prereqs."

# add repo for docker-ce
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# add repo for gitlab-runner
curl -sSL https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# add repo for yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

# add repo for node
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -

# update again after adding repos
sudo apt-get update -yqq

# install important bits
echo "Installing docker & gitlab-runner..."
sudo apt-get install -yqq \
    docker-ce \
    gitlab-runner
echo "Done installing docker/gitlab-runner."

sudo usermod -aG docker gitlab-runner

sudo curl -sSL \
    "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

echo "Installing build deps..."
# install build deps
sudo aptitude install -yq \
    build-essential \
    libzmq3-dev \
    libssl-dev \
    pkg-config \
    protobuf-compiler \
    openssl \
    nodejs \
    npm \
    yarn
echo "Done installing build deps."

sudo mkdir -p /home/gitlab-runner/.docker
sudo chown gitlab-runner:gitlab-runner /home/gitlab-runner/.docker

if no_exe rustup; then
    sudo curl -sSL https://sh.rustup.rs -o /usr/local/bin/rustup-init
    sudo chmod +x /usr/local/bin/rustup-init
    sudo -iu gitlab-runner rustup-init -y
    sudo mkdir -p $RUNNER_HOME
    sudo chown gitlab-runner:gitlab-runner $RUNNER_HOME
    sudo -u gitlab-runner touch $RUNNER_HOME/.profile
    echo 'source $HOME/.cargo/env' | sudo -u gitlab-runner tee -a $RUNNER_HOME/.profile
fi
