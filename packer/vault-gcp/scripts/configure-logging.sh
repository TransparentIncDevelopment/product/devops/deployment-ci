#!/usr/bin/env bash

set -o nounset
set -o errexit

function helptext () {
    local text=$(cat <<EOF
    Configures rsyslog to capture systemd logs that come from the vault service to Loggly.

    Arguments
        LOGGLY_TOKEN (Required) = Authentication token for accessing Loggly account.
        WORKSPACE (Required) = Terraform workspace.
        COMPONENT (Required) = Name of local component for which logs will be gathered.
EOF
          )
    echo "$text"
}

function error () {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

if [[ $# -eq 0 ]]; then
    error "Not enough arguments provided"
fi

# Variables
LOGGLY_TOKEN=${1:?"$(error 'LOGGLY_TOKEN must be set')"}
WORKSPACE=${2:?"$(error 'WORKSPACE must be set')"}
COMPONENT=${3:?"$(error 'COMPONENT must be set')"}

SERVER_ADDR=$(hostname -i)

sudo tee /etc/rsyslog.d/60-server.conf <<EOF
# Setup Loggly logging template
template(name="LogglyFormat" type="string"
  string="<%pri%>%protocol-version% %timestamp:::date-rfc3339% %HOSTNAME% %app-name% %procid% %msgid% [${LOGGLY_TOKEN}@41058 tag=\"${WORKSPACE}\" tag=\"GCP-${SERVER_ADDR}-${WORKSPACE}-${COMPONENT}\"] %msg%\n")

# Send logs to Loggly
*.* action(type="omfwd" protocol="tcp" target="logs-01.loggly.com" port="514" template="LogglyFormat")


EOF

sudo systemctl restart rsyslog.service
