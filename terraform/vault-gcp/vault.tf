
resource "google_compute_instance" "vault" {
  name = "hashicorp-vault-${terraform.workspace}"
  machine_type = "n1-standard-1"
  zone = "${var.zone}"
  allow_stopping_for_update = true

  tags = [ var.tag_name ]

  metadata_startup_script = data.template_file.startup-script-config.rendered

  connection {
    agent = false
    timeout = "10m"
  }

  boot_disk {
    initialize_params {
      image = "${data.google_compute_image.vault_image.self_link}"
    }
  }

  network_interface {
    network = "${var.network}"
    subnetwork = "${var.private_subnet}"
  }

  service_account {
    scopes = [ "compute-ro", "storage-rw", "https://www.googleapis.com/auth/cloudkms" ]
  }
}
