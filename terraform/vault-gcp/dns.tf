data "google_dns_managed_zone" "vault_dns" {
  name = var.dns_zone
}

resource "google_dns_record_set" "dns" {
  name = "vault.${data.google_dns_managed_zone.vault_dns.dns_name}"
  managed_zone = "${data.google_dns_managed_zone.vault_dns.name}"
  type = "A"
  ttl = 300

  rrdatas = ["${google_compute_forwarding_rule.vault-lb-forwarding-rule.ip_address}"]
}
