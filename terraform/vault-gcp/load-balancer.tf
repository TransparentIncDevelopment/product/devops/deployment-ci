resource "google_compute_instance_group" "vault-ig" {
  name = "vault-ig"

  instances = [
    "${google_compute_instance.vault.self_link}"
  ]

  zone = "${var.zone}"
}

resource "google_compute_health_check" "vault-health-check" {
  name = "vault-health-check"

  tcp_health_check {
    port = "8200"
  }
}

resource "google_compute_region_backend_service" "vault-lb" {
  name = "vault-lb"
  health_checks = ["${google_compute_health_check.vault-health-check.self_link}"]
  region = "${var.region}"

  backend {
    group = "${google_compute_instance_group.vault-ig.self_link}"
  }
}

resource "google_compute_forwarding_rule" "vault-lb-forwarding-rule" {
  name = "vault-lb-forwarding-rule"
  load_balancing_scheme = "INTERNAL"
  ports = ["8200"]
  network = "${var.network}"
  subnetwork = "${var.private_subnet}"
  backend_service = "${google_compute_region_backend_service.vault-lb.self_link}"
}

resource "google_compute_firewall" "allow-health-check" {
  name    = "allow-vault-health-check"
  network = "${var.network}"

  allow {
    protocol = "tcp"
  }

  # Ranges come from: https://cloud.google.com/load-balancing/docs/https/#firewall_rules
  # Also adding anything from the private CIDR ranges of 10.0.0.0 it would be
  # better to use only our private clusters subnet, but source ips seem to also
  # be coming from kubernetes virtual network so we need to open it up a bit more
  # unless a different solution can be come up with from talking with GCP.
  source_ranges = ["130.211.0.0/22", "35.191.0.0/16", "10.0.0.0/8" ]
  target_tags   = ["vault-lb", var.tag_name]
}
