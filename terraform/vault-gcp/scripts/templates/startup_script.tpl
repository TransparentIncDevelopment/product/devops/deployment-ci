#!/usr/bin/env bash

set -o nounset
set -o errexit

sudo /etc/scripts/configure-vault.sh ${vault_bucket}
/etc/scripts/configure-logging.sh ${loggly_token} ${workspace} ${component}
