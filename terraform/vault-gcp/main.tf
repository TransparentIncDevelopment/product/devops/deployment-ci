provider "google" {
  project = "${var.project_id}"
  region = "${var.region}"
}

terraform {
  backend "gcs" {
    prefix = "vault"
  }
}
