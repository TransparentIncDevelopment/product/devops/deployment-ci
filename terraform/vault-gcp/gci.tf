data "google_compute_image" "vault_image" {
  family = "transparent-vault"
  project = "${var.project_id}"
}
