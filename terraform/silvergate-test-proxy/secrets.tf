provider "vault" {
  address = "http://52.25.23.45:8200"
  skip_tls_verify = true
}

data "vault_generic_secret" "deployer-key-pub" {
  path = "secret/environments/dev/deployer-key_pub"
}

data "vault_generic_secret" "deployer-key" {
  path = "secret/environments/dev/deployer-key"
}