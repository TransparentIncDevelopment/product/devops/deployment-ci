terraform {
  backend "s3" {
    bucket = "transparent-terraform"
    key = "silvergate-test-proxy"
    region = "us-west-2"
  }
}
