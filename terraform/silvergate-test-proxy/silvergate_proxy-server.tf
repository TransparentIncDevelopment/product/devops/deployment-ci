provider "aws" {
  region = "${var.region}"
}

resource "aws_key_pair" "deployer" {
  key_name = "sg_proxy_deployer_key"
  public_key = "${data.vault_generic_secret.deployer-key-pub.data["value"]}"
}

data "aws_ami" "nginx_ami" {
  filter {
    name = "state"
    values = ["available"]
  }

  filter {
    name = "tag:Component"
    values = ["nginx"]
  }

  most_recent = true
  owners = ["self"]
}

resource "aws_security_group" "silvergate_proxy_security_group" {
  name = "silvergate_proxy_security_group"
  description = "Silvergate Proxy Security Group"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "${data.terraform_remote_state.default-vpc.aws_vpc_test_nets_cidr_block}",
      "${data.terraform_remote_state.default-vpc.aws_vpc_default_cidr_block}"
    ]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "${data.terraform_remote_state.default-vpc.aws_vpc_test_nets_cidr_block}",
      "${data.terraform_remote_state.default-vpc.aws_vpc_default_cidr_block}"
    ]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "silvergate_proxy_server" {
  ami = "${data.aws_ami.nginx_ami.id}"
  instance_type = "t3.micro"
  subnet_id = "${data.terraform_remote_state.default-vpc.private_subnet_id}"
  vpc_security_group_ids = ["${aws_security_group.silvergate_proxy_security_group.id}"]
  associate_public_ip_address = false
  key_name = "${aws_key_pair.deployer.key_name}"

  tags {
    Name = "Silvergate Proxy Server"
  }

  root_block_device {
    volume_size = "100"
  }

  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = "${data.vault_generic_secret.deployer-key.data["value"]}"
  }

  provisioner "file" {
    source = "nginx.conf"
    destination = "/tmp/nginx.conf"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/nginx.conf /etc/nginx/nginx.conf",
      "sudo nginx -s reload"
    ]
  }
}
