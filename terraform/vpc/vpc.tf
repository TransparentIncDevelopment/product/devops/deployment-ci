provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_key_pair" "deployer" {
  key_name = "deployer_key"
  public_key = "${data.vault_generic_secret.deployer-key-pub.data["value"]}"
}

### VPC ###

data "aws_vpc" "default" {
  id = "${var.default_vpc_id}"
}

data "aws_vpc" "test_nets" {
  id = "${var.test_nets_vpc_id}"
}

### Internet Gateway ###

data "aws_internet_gateway" "default_nets-gw" {
  internet_gateway_id = "${var.default-gw_id}"
}

### NAT Gateway ###

resource "aws_eip" "default_nat_eip" {
  vpc = true
  depends_on = ["data.aws_internet_gateway.default_nets-gw"]

  tags {
    Name = "default_nat_eip"
  }

  # This is a safeguard in case someone tries to destroy a vpc
  # since this ip address can be used in whitelisting.
  # If you still want to delete the vpc then set prevent_destroy to false
  # and make sure not to commit this into source control.
  lifecycle {
    prevent_destroy = "true"
  }
}

resource "aws_nat_gateway" "default_nat" {
  allocation_id = "${aws_eip.default_nat_eip.id}"
  subnet_id = "${aws_subnet.public_subnet.id}"
  depends_on = ["data.aws_internet_gateway.default_nets-gw"]

  tags {
    Name = "default_nat"
  }
}

### Public Subnet ###

resource "aws_subnet" "public_subnet" {
  vpc_id = "${data.aws_vpc.default.id}"
  cidr_block = "${var.public_subnet_cidr}"
  depends_on = ["data.aws_internet_gateway.default_nets-gw"]
  availability_zone = "${var.availability_zone}"

  tags {
    Name = "default_public_subnet"
  }
}

### Private Subnet ###

resource "aws_subnet" "private_subnet" {
  cidr_block = "${var.private_subnet_cidr}"
  vpc_id = "${data.aws_vpc.default.id}"
  depends_on = ["data.aws_internet_gateway.default_nets-gw"]
  availability_zone = "${var.availability_zone}"

  tags {
    Name = "default_private_subnet"
  }
}

resource "aws_route_table" "private_route_table" {
  vpc_id = "${data.aws_vpc.default.id}"

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.default_nat.id}"
  }
}

# !! This wasn't tested before committed, manually done to retain whitelisted eip
resource "aws_route_table_association" "private_route_table_association" {
  subnet_id = "${aws_subnet.private_subnet.id}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}


