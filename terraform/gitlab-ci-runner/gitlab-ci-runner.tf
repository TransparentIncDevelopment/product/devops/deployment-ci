provider "aws" {
  region = "${var.region}"
}

resource "aws_key_pair" "ci_deployer" {
  key_name = "ci-deployer-key"
  public_key = "${file("ci-deployer-key.pub")}"
}

data "aws_ami" "node_ami" {
  filter {
    name = "state"
    values = ["available"]
  }

  filter {
    name = "tag:Component"
    values = ["gitlab-ci-runner"]
  }

  most_recent = true
  owners = ["self"]
}

resource "aws_security_group" "ci_runner_security_group" {
  name = "ci_runner_security_group"
  description = "CI Runner Security Group"

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "gitlab_integration_runner" {
  ami = "${data.aws_ami.node_ami.id}"
  instance_type = "c5.xlarge"
  vpc_security_group_ids = ["${aws_security_group.ci_runner_security_group.id}"]
  associate_public_ip_address = true
  key_name = "${aws_key_pair.ci_deployer.key_name}"
  count = "6"

  tags {
    Name = "GitLab Integration Runner"
  }

  root_block_device {
    volume_size = "100"
  }

  connection {
    type = "ssh"
    user = "ubuntu"
    agent = false
    private_key = "${file("ci-deployer-key")}"
  }

  provisioner "file" {
    source = "scripts/start-integration-runner.sh"
    destination = "/tmp/start-integration-runner.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod 755 /tmp/start-integration-runner.sh",
      "/tmp/start-integration-runner.sh ${var.project_registration_token}"
    ]
  }
}

resource "aws_instance" "gitlab_docker_runner" {
  ami = "${data.aws_ami.node_ami.id}"
  instance_type = "c5.xlarge"
  vpc_security_group_ids = ["${aws_security_group.ci_runner_security_group.id}"]
  associate_public_ip_address = true
  key_name = "${aws_key_pair.ci_deployer.key_name}"

  count = "10"
  tags {
    Name = "GitLab Docker Runner"
  }

  root_block_device {
    volume_size = "100"
  }

  connection {
    type = "ssh"
    user = "ubuntu"
    agent = false
    private_key = "${file("ci-deployer-key")}"
  }

  provisioner "file" {
    source = "scripts/start-standard-runner.sh"
    destination = "/tmp/start-standard-runner.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod 755 /tmp/start-standard-runner.sh",
      "/tmp/start-standard-runner.sh ${var.project_registration_token}"
    ]
  }
}
