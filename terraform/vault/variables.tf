variable "region" {
  default = "us-west-2"
}

variable "vpn_vpc_id" {
    default = "vpc-9a7b4efd"
}

variable "private_subnet_id" {
    default = "subnet-e26b2a85"
}