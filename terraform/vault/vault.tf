resource "aws_security_group" "vault_security_group" {
  name = "${terraform.workspace}-vault_security_group"
  vpc_id = "${var.vpn_vpc_id}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 8200
    to_port = 8200
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "vault" {
  vpc = true
  tags {
    Name = "${terraform.workspace}-vault"
  }
}

resource "aws_eip_association" "vault_eip_assoc" {
  instance_id   = "${aws_instance.vault.id}"
  allocation_id = "${aws_eip.vault.id}"
}

resource "aws_instance" "vault" {
  ami = "${data.aws_ami.vault_ami.id}"
  instance_type = "t2.medium"
  iam_instance_profile = "xand-vault-role"
  subnet_id = "${var.private_subnet_id}"

  tags {
    Name = "hashicorp-vault-${terraform.workspace}"
    Component = "vault"
  }

  count = "1"

  vpc_security_group_ids = [
    "${aws_security_group.vault_security_group.id}"
  ]

  key_name = "${terraform.workspace}-deployer_key"

  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = "${file("deployer-key")}"
    agent = false
    timeout = "10m"
  }

  provisioner "file" {
    source = "config.hcl"
    destination = "/tmp/config.hcl"
  }

  provisioner "file" {
    source = "scripts/configure-logging.sh"
    destination = "/tmp/configure-logging.sh"
  }

  provisioner "file" {
    source = "services/vault.service"
    destination = "/tmp/vault.service"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/vault.service /lib/systemd/system",
      "sudo chown root:root /lib/systemd/system/vault.service",
      "sudo mkdir -p /etc/vault",
      "sudo mv /tmp/config.hcl /etc/vault/config.hcl",
      "sudo chown root:root /etc/vault/config.hcl",
      "chmod 755 /tmp/configure-logging.sh",
      "/tmp/configure-logging.sh ${self.private_ip} ${data.aws_kms_secrets.vault_store.plaintext["loggly-acct-token"]} ${terraform.workspace} ${self.tags.Component}",
      "sudo systemctl enable vault.service",
      "sudo systemctl start vault.service",
      "sudo systemctl disable ssh",
      "sudo systemctl stop ssh"
    ]
  }
}
