storage "s3" {
  bucket = "xand-vault-secrets-stash"
  region = "us-west-2"
}

listener "tcp" {
 address     = "0.0.0.0:8200"
 tls_disable = 1
}