#!/usr/bin/env bash

set -e

# install docker
sudo apt-get update
sudo apt install docker.io -yqq
sudo systemctl start docker
sudo systemctl enable docker

# Download docker-credential-gcr helper. Reference:
# https://cloud.google.com/container-registry/docs/advanced-authentication#standalone-helper
echo $USER
VERSION=2.0.0
OS=linux  # or "darwin" for OSX, "windows" for Windows.
ARCH=amd64  # or "386" for 32-bit OSs, "arm64" for ARM 64.
TAR_NAME="docker-credential-gcr.tar.gz"
EXE_NAME="docker-credential-gcr"
curl -SL -o $TAR_NAME "https://github.com/GoogleCloudPlatform/docker-credential-gcr/releases/download/v${VERSION}/docker-credential-gcr_${OS}_${ARCH}-${VERSION}.tar.gz"
tar xzf $TAR_NAME
sudo cp $EXE_NAME /usr/bin/docker-credential-gcr
sudo chmod ugo+rwx /usr/bin/docker-credential-gcr

# install gitlab runner
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
sudo dpkg -i gitlab-runner_amd64.deb

# Add gitlab-runner user to docker
sudo usermod -aG docker gitlab-runner

