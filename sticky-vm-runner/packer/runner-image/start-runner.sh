#!/usr/bin/env bash

set -e
if [[ -z $GITLAB_REGISTRATION_TOKEN ]] ; then
    echo "ERROR: GITLAB_REGISTRATION_TOKEN must be set. Set it in the environment or before calling script with GITLAB_REGISTRATION_TOKEN=xxx ./build.sh"
    exit 1
fi

# Configure gcr credential helper for root user
# Note: Adding this actually enables the jobs to auth and pull docker rimages
sudo mkdir -p /root/.docker/
sudo tee -a /root/.docker/config.json <<EOF
{
	"auths": {},
	"credHelpers": {
		"asia.gcr.io": "gcr",
		"eu.gcr.io": "gcr",
		"gcr.io": "gcr",
		"marketplace.gcr.io": "gcr",
		"us.gcr.io": "gcr"
	}
}
EOF

# Start gitlab-runner
sudo gitlab-runner register \
  --name="sticky-docker-runner" \
  --non-interactive \
  --url https://gitlab.com/ \
  --executor docker \
  --docker-tlsverify=true \
  --docker-privileged=false \
  --docker-disable-entrypoint-overwrite=false \
  --docker-oom-kill-disable=false \
  --docker-disable-cache=false \
  --docker-image alpine:latest \
  --builds-dir "/build" \
  --docker-volumes "/build:/build" \
  --cache-dir "/cache" \
  --docker-volumes "/cache:/cache" \
  --custom_build_dir-enabled=true \
  --run-untagged=false \
  --registration-token $GITLAB_REGISTRATION_TOKEN \
  --tag-list sticky-docker-runner \
  --paused=true

sudo gitlab-runner restart


