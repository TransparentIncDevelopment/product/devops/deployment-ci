variable "vm_name" {
  default = "sticky-vm-runner"
}

variable "gcp_project" {
  default = "xand-dev"
}

variable "description" {
  default = "GitLab runner with persistence storage for optimized cargo caching"
}

variable "disk_size" {
  default = 500 # Size in GB
}

# Machine type must be available in zone
# List available machine types with:
variable "machine_type" {
  default = "n1-highcpu-64" # 64 cores + 57 GB ram
}

variable "zone" {
  default = "us-central1-b"
}

variable "image_project" {
  default = "xand-dev"
}

variable "image_family" {
  default = "transparent-gitlab"
}

variable "network" {
  default = "projects/xand-dev/global/networks/default"
}

variable "subnet" {
  default = "projects/xand-dev/regions/us-central1/subnetworks/private-clusters"
}
