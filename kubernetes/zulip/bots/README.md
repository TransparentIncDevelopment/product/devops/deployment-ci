# Installing zulip bots

For all zulip bots you will need to create a bot under your profile and select download to download a zuliprc file to configure the bots with. See https://zulipchat.com/api/running-bots for more information.

Mostly all you have to do is apply the `deployment.yml` file which should have all the bots necessary to be deployed as long as all of the secrets have been configured.

## Giphy Secret

So you'll need the giphy-zuliprc and giphy.conf files to create the zulip secret. Either you'll have to create a bot from in the zulip app, and make your own giphy.conf by creating a giphy account and create an api key or you can copy from an existing kubernetes configuration the existing configuration by running the bot below:

```shell
kubectl get secret giphy-bot -o "jsonpath={.data['giphy-zuliprc']}" | base64 -d > giphy-zuliprc
kubectl get secret giphy-bot -o "jsonpath={.data['giphy\.conf']}" | base64 -d > giphy.conf
```

From here it's just a matter of creating the secret in the new kubernetes cluster or deleting the existing one to then deploy an updated version.

```shell
# run the delete if the secret already exists.
kubectl delete secret giphy-bot
kubectl create secret generic giphy-bot --from-file=./giphy-zuliprc --from-file=./giphy.conf
```

Then remember to either deploy for the first time or delete the exist pod so it's recreated with the new secret.

## Deploying changes to the docker build

If you want to update the image for zulip-bots. You'll probably want to version it, but here's how it was built originally:

```
gcloud docker -a
docker build -t gcr.io/xand-dev/zulip-bots:latest .
docker push gcr.io/xand-dev/zulip-bots:latest
```

## Adding additional bots

Take a look here and see if there's a bot that you want to include: https://github.com/zulip/python-zulip-api/tree/master/zulip_bots/zulip_bots/bots

You'll want to ammend the deployment.yml file in order to add containers or a separate deployment for the new instance of deployment that you want to run. Then you just need to create a secret for the corresponding bot with the general zuliprc file and potentially a bot specific file.

```shell
kubectl create secret generic ${bot}-bot --from-file=./${bot}-zuliprc --from-file=./${bot}.conf
```

Finally just include the bot name and configuration in the yaml.

```yml
      - name: ${bot}-bot
        image: gcr.io/xand-dev/zulip-bots:latest
        command: ["zulip-run-bot"]
        args: ["${bot}", "--config-file", "/secrets/${bot}-zuliprc", "--bot-config-file", "/secrets/${bot}.conf"]
        volumeMounts:
          - name: ${bot}-bot
            mountPath: /secrets/
        resources:
          limits:
            cpu: 10m
            memory: 256Mi
      volumes:
      - name: ${bot}-bot
        secret:
          secretName: ${bot}-bot
```
