# Running and Installing zulip

Instructions for installing and running zulip are pretty straight-forward by just using convention. Following applying kubernetes configuration in order based on the starting numbers of the files. The template file needs to be replaced with the corresponding values. In case you don't know what those values are. You can just output the values from the existing secrets. 

`kubectl get secret zulip --output yaml > secret-export.yml`

So copy the file and remove the `.template` extension then replace the values with what was retrieved above. Feel free to base64 decode some of the values to make sure they are valid values.

Then just continue to `kubectl apply -f 00-pvc.yml` in order until you reach the end of the files excluding any files ending in `.template`.

# Recovering From Failure

At this point if the postgresql instance goes down for any reason when it comes back up the entire zulip schema is lost. I think this has to do with not having WAL logs, but I can't be sure and you can [read more here](https://www.postgresql.org/docs/9.4/wal.html). However that means that recovery will likely be needed more often than we'd like. So here are the steps for recovery.

Most likely you'll just want to recover from the latest dump which would be from the disk attached to the zulip instance which is mounted at /backups. However I'll write a section below that you can skip if you'd prefer to pull from a snapshot at a certain point in time.

## Attaching a Snapshot Image Disk to Recover From

As mentioned above this is only needed if you don't want to use the latest backup as that is already attached to the zulip instance.

```shell
# Choose a snapshot to recover from
gcloud compute snapshots list | grep zulip-backup
# Create a disk from that snapshot
gcloud compute disks create zulip-snapshot-backup --size 160 --source-snapshot {snapshot_name} --type pd-standard
```

After the disk is created then you just need to attach it to the zulip pod so that you can access it running in zulip.
For this you'll want to edit the zulip deployment in order to add a volume and a volume mount.

```shell
# Either edit deployment.yaml and run `kubectl apply -f 02-deployment.yaml` or
# edit using `kubectl edit` via the line below which uses your $EDITOR to edit in place.
kubectl edit deployment zulip
```

You'll then want to add the `zulip-snapshot-backup` items to the `volumeMount` and `volumes` section for the zulip deployment.

Afterwards it should look something like below.
```yaml
        volumeMounts:
          - name: zulip-persistent-storage
            mountPath: /data
          - name: zulip-backup-storage
            mountPath: /backups
          - name: zulip-snapshot-backup
            mountPath: /snapshotsBackups
....
      volumes:
      - name: zulip-persistent-storage
        persistentVolumeClaim:
          claimName: zulip
      - name: zulip-backup-storage
        persistentVolumeClaim:
          claimName: zulip-backup
      - name: zulip-snapshot-backup
        gcePersistentDisk:
          pdName: zulip-snapshot-backup
          fsType: ext4
```

You should clean this up after the restore is complete by editing this out again which will restart zulip. (Remember to do this before letting others start using the system. Failing to do this will lead to confusion on subsequent runs of this since the disk name would remain and we would be charged for a disk that is not being used.

After removing it from zulip deployment, and the disk is detached. Last thing you'll want to do is actually delete the disk.

```shell
gcloud compute disks delete zulip-snapshot-backup
```

## Actually performing the recovery

In case the postgresql instance is completely gone it's probably better to start with an empty slate.

```shell
# Retrieve the name of the postgresql pod
kubectl get pods
kubectl exec -it postgresql-{from above} /bin/bash
# Below will check if the postgresql zulip schema is empty
psql -U zulip
\dt
\q

# If there were no relations just clean out the existing data directory to avoid conflicts with data recovery
rm -rf /var/lib/postgresql/data/pgdata/*
# Quit back to your terminal
exit
# Deleting the pod will cause a new one to be created which should use the empty data directory
kubectl delete pod postgresql-{from above}
```

```shell
# Retrieve the name of the zulip pod
kubectl get pods
kubectl exec -it zulip-{from above} /bin/bash
# Switch to the zulip user since manage.py needs to use the zulip user.
su zulip
cd ~/deployments/current

# If postgresql is empty and you are trying to recover from a 
# failure rather than restore to a certain point. Then you'll need
# to initialize database otherwise skip this step.
./scripts/install/initialize-database

# The default realm that points directly to chat.tpfs.io is '' so that's what is being used here. However if we have additional realms later this can be modified by replacing the ''.

# Deactivate the realm is only needed if this isn't from a failure.
./manage.py deactivate_realm -r ''

# Import from the backup you want either /backups or /snapshotsBackups
./manage.py import '' /backups/zulip/

# Reactivate Realm after completion (this is needed either way)
./manage.py reactivate_realm -r ''
```
