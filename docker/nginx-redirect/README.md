# HTTPS Redirect

First, install and confiure the Google Cloud (GCP) SDK from [here](https://cloud.google.com/sdk/docs/).

Set Docker as GCP's credential helper;
```
gcloud auth configure-docker
```
Then, install the Docker credential helper:
```
gcloud components install docker-credential-gcr
```
>If this is not successful, use methods described on [GCP's site](https://cloud.google.com/container-registry/docs/advanced-authentication). 

Configure Docker to use your Container Registry credentials (one time only):
```shell script
docker-credential-gcr configure-docker
```

To compile this image and have it available you'll need to build it locally and then publish it.
```shell
docker build -t gcr.io/xand-dev/nginx-redirect:1.0.0 .
# Then tag it to a version number and make that version the latest for
# the remote repository.
docker tag gcr.io/xand-dev/nginx-redirect:1.0.0 gcr.io/xand-dev/nginx-redirect:latest
# Finally push it to the google cloud private repo.
docker push gcr.io/xand-dev/nginx-redirect
