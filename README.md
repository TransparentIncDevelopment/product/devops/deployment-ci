# deployment-ci

[![pipeline status](https://gitlab.com/TransparentIncDevelopment/deployment-ci/badges/master/pipeline.svg)](https://gitlab.com/TransparentIncDevelopment/deployment-ci/commits/master)

This repository contains:

* Resources for deploying a GitLab CI Runner for running [`sawyer`](https://gitlab.com/TransparentIncDevelopment/sawyer)'s integration tests.  This machine is pre-provisioned with the necessary dependencies for building and running `sawyer`'s `docker-compose`-based Integration Tests.
* Resources for deploying a Vault instance on AWS.

### Project License

MIT OR Apache-2.0

### Environment

In order to run `terraform apply` for the GitLab CI Runner, the following environment variable **can** be set:

* `TF_VAR_project_registration_token`: This should be set to the GitLab CI Registration Token (see [here](https://docs.gitlab.com/ce/ci/runners/)).

If this variable is not set in the environment, `terraform` will prompt the user for its value.

*Note: This variable is unused when using terraform to manage the Vault deployment.*

### Credentials

In addition, the following environment variables **must** be set:

* `AWS_ACCESS_KEY_ID`
* `AWS_SECRET_ACCESS_KEY`

**or**

* you must have a `$HOME/.aws/credentials` file.

### Generating an AMI

To generate an AMI, simply run `packer build *.json` from within the target folder.

#### Example: GitLab CI Runner

The current `gitlab-ci-runner` image is based off Ubuntu 18.04 LTS.

```sh
$ cd packer/gitlab-ci-runner
$ packer build gitlab-ci-runner.json
```

### Terraforming

(must be done after generating the corresponding AMI)

```sh
$ # Example for deploying GitLab CI Runner
$ # To deploy a Vault instance, run the same commands from the terraform/vault directory.
$ cd terraform/gitlab-ci-runner
$ ssh-keygen -t rsa -b 4096 -f ci-deployer-key   # first time only
$ yes yes | (terraform plan && terraform apply)
```

The runner should be automatically available next time a job is run.  Check `Settings:CI/CD:Runners` on GitLab to verify.
