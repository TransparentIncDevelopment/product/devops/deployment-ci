
#Revokes all token accessors from a file

FILE=${1:?"You must provide a file with accessors"}

while read ACCESSOR; do
  vault token revoke -accessor $ACCESSOR
done <$FILE