#!/usr/bin/env bash

set -e

export VAULT_TOKEN=${1:?"One time token required"}
vault read --tls-skip-verify cubbyhole/$VAULT_TOKEN
unset VAULT_TOKEN

echo "Run 'vault login --tls-skip-verify ' using the token value returned by 'vault read' above when prompted. Then verify that the new token is copied into your '~/.vault-token' file."
